package swredis

import (
	"github.com/go-redis/redis/v8"
)

type SWRedis struct {
	rdsClient *redis.Client
}

type RedisData struct {
	Expire  uint32 `json:"expire"`
	AddTime uint32 `json:"addtime"`
	Data    string `json:"data"`
}

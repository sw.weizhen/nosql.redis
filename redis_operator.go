package swredis

import (
	"fmt"
	"time"
)

func (ref *SWRedis) Ping() (string, error) {

	pong, err := ref.rdsClient.Ping(ctx).Result()
	if err != nil {
		return "", err
	}

	return pong, nil
}

func (ref *SWRedis) Del(key string) error {
	i := ref.rdsClient.Del(ctx, key)

	return i.Err()
}

func (ref *SWRedis) Set(key string, data interface{}, expire int32) error {
	err := ref.rdsClient.Set(ctx, key, data, time.Duration(time.Second*time.Duration(expire))).Err()

	return err
}

func (ref *SWRedis) Get(key string) string {
	resp := ref.rdsClient.Get(ctx, key)
	if resp == nil {
		return ""
	}

	return resp.Val()
}

func (ref *SWRedis) SetNX(key string, data interface{}, expire int32) bool {
	resp := ref.rdsClient.SetNX(ctx, key, data, time.Duration(expire)*time.Second)
	if resp == nil {
		return false
	}

	if _, err := resp.Result(); err != nil {
		return false
	}

	return resp.Val()
}

func (ref *SWRedis) IsKeyExists(key string) bool {
	result := ref.rdsClient.Exists(ctx, key)

	return result.Val() == 1
}

func (ref *SWRedis) Close() {
	if ref.rdsClient != nil {
		ref.rdsClient.Close()
	}
}

func (ref *SWRedis) FormatSet(key string, data string, expire int32) error {
	ftData := formatData(data, expire)
	err := ref.rdsClient.Set(ctx, key, ftData, time.Duration(time.Second*time.Duration(expire))).Err()

	return err
}

func (ref *SWRedis) FormatGet(key string) *RedisData {

	resp := ref.rdsClient.Get(ctx, key)
	if resp == nil {
		return nil
	}

	rtn := extractData(resp.Val())

	return rtn
}

func (ref *SWRedis) HSet(hashkey string, field string, value interface{}) error {
	resp := ref.rdsClient.HSet(ctx, hashkey, field, value)
	if resp == nil {
		return fmt.Errorf("HSet nil pointer")
	}

	if _, err := resp.Result(); err != nil {
		return err
	}

	return nil
}

func (ref *SWRedis) HGet(hashkey, field string) (string, error) {
	resp := ref.rdsClient.HGet(ctx, hashkey, field)
	if resp == nil {
		return "", fmt.Errorf("HGet nil pointer")
	}

	if _, err := resp.Result(); err != nil {
		return "", err
	}

	return resp.Val(), nil
}

func (ref *SWRedis) HGetAll(hashkey string) (map[string]string, error) {
	resp := ref.rdsClient.HGetAll(ctx, hashkey)
	if resp == nil {
		return make(map[string]string), fmt.Errorf("HGetAll nil pointer")
	}

	if _, err := resp.Result(); err != nil {
		return make(map[string]string), err
	}

	return resp.Val(), nil
}

func (ref *SWRedis) HDel(hashkey, field string) (int, error) {
	resp := ref.rdsClient.HDel(ctx, hashkey, field)
	if resp == nil {
		return 0, fmt.Errorf("HDel nil pointer")
	}

	if _, err := resp.Result(); err != nil {
		return 0, err
	}

	return int(resp.Val()), nil
}

func (ref *SWRedis) HExists(hashkey, field string) (bool, error) {
	resp := ref.rdsClient.HExists(ctx, hashkey, field)
	if resp == nil {
		return false, fmt.Errorf("HExists nil pointer")
	}

	if _, err := resp.Result(); err != nil {
		return false, err
	}

	return resp.Val(), nil
}

func (ref *SWRedis) HKeys(hashkey string) ([]string, error) {
	resp := ref.rdsClient.HKeys(ctx, hashkey)
	if resp == nil {
		return make([]string, 0), fmt.Errorf("HKeys nil pointer")
	}

	if _, err := resp.Result(); err != nil {
		return make([]string, 0), err
	}

	return resp.Val(), nil
}

func (ref *SWRedis) HLen(hashkey string) (int, error) {
	resp := ref.rdsClient.HLen(ctx, hashkey)
	if resp == nil {
		return 0, fmt.Errorf("HLen nil pointer")
	}

	if _, err := resp.Result(); err != nil {
		return 0, err
	}

	return int(resp.Val()), nil
}

func (ref *SWRedis) HMSet(hashkey string, values map[string]interface{}) (bool, error) {
	resp := ref.rdsClient.HMSet(ctx, hashkey, values)
	if resp == nil {
		return false, fmt.Errorf("HMSet nil pointer")
	}

	if _, err := resp.Result(); err != nil {
		return false, err
	}

	return resp.Val(), nil
}

func (ref *SWRedis) HMGet(hashkey string, fields ...string) ([]interface{}, error) {
	if len(fields) <= 0 {
		return make([]interface{}, 0), fmt.Errorf("wrong number of arguments")
	}

	resp := ref.rdsClient.HMGet(ctx, hashkey, fields...)
	if resp == nil {
		return make([]interface{}, 0), fmt.Errorf("HMGet nil pointer")
	}

	if _, err := resp.Result(); err != nil {
		return make([]interface{}, 0), err
	}

	return resp.Val(), nil
}

module gitlab.com/sw.weizhen/nosql.redis

go 1.16

require github.com/go-redis/redis/v8 v8.11.0
